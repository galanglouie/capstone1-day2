// DOM Elements
const time = document.getElementById('time'),
  greeting = document.getElementById('greeting'),
  logo = document.getElementById('logo'); 

// Options
const showAmPm = true;

// Show Time
function showTime() {
  let today = new Date(),
    hour = today.getHours(),
    min = today.getMinutes(),
    sec = today.getSeconds();

  // Set AM or PM
  const amPm = hour >= 12 ? 'PM' : 'AM';

  // 12hr Format
  hour = hour % 12 || 12;

  // Output Time
  time.innerHTML = `${hour}<span>:</span>${addZero(min)}<span>:</span>${addZero(
    sec
  )} ${showAmPm ? amPm : ''}`;

  setTimeout(showTime, 1000);
}

// Add Zeros
function addZero(n) {
  return (parseInt(n, 10) < 10 ? '0' : '') + n;
}

// Set Background and Greeting
function setBgGreet() {
  let today = new Date(),
    hour = today.getHours();

  if (hour < 12) {
    // Morning
    document.body.style.backgroundImage = "url('assets/images/tools.jpg')";
    greeting.textContent = 'Good Morning, ';
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundSize = "cover";
    logo.src = ('assets/images/Brasomon_LOGO-HQ.png');
    
    
  } else if (hour < 18) {
    // Afternoon
    document.body.style.backgroundImage = "url('assets/images/tools2.jpg')";
    greeting.textContent = 'Good Afternoon, ';
    document.body.style.color = 'white';
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundSize = "cover";
  } else {
    // Evening
    document.body.style.backgroundImage = "url('assets/images/Stencil.jpg')";
    greeting.textContent = 'Good evening, ';
    document.body.style.color = 'white';
    document.body.style.backgroundRepeat = "no-repeat";
    document.body.style.backgroundSize = "cover";
  }
}



// Run
showTime();
setBgGreet();

